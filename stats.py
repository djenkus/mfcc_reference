#!/usr/bin/env python

from python_speech_features import mfcc
from python_speech_features import delta
from python_speech_features import logfbank
import scipy.io.wavfile as wav
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
import os
#import librosa
import shutil 


#dataset folder 
testAudioDataSetDIR = "./dataset/"
# wav audio files
testAudioDIR = "./testAudio"
# converted wav files to txt for MSP SD card
testAudioRawDIR = "./testAudioRaw"
# MFCCs 2-13 from MSP
mspMFCCDIR = "./mspMFCC/"
# MFCCs 1-13 from MSP
rawMSPMFCCDIR = "./rawMSPMFCC/"
# python reference MFCCs
refMFCCDIR = "./refMFCC/"
# folder for comparison results
statsDIR = "./stats/"
# list all files in dataset folder
testAudioDataSetFiles = os.listdir(testAudioDataSetDIR)

refMFCCFiles = os.listdir(refMFCCDIR)
testAudioFiles = os.listdir(testAudioDIR)
rawMspMFCCFiles = os.listdir(rawMSPMFCCDIR)
mspMFCCFiles = os.listdir(mspMFCCDIR)

#print(mspMFCCFiles)
#print(rawMspMFCCFiles)

rate = 8000
winlen=0.032
winstep=0.012
numcep=13
nfilt=26
nfft=256
lowfreq=0
highfreq=None
preemph=0.97
ceplifter=0
appendEnergy=False
winfunc=np.hamming

overlap = 100.0 *((winlen - winstep) / winlen )
#print(overlap)

# read MFCCs from txt file to list
def readFeatures(mfcc_file, fpath):
    featData = []
    path = fpath + mfcc_file
    f = open(path, "r")
    for x in f:
        tmp = x.split(" ")[:-1]
        tmpFloat = []
        featData.append(list(np.float_(x.split(" ")[:-1])))
    return featData

def downSampleAudio(audioData, rate, new_rate=8000):
    factor  = rate // new_rate
    new_fs = rate // factor
    index = range(0, audioData.size, factor)
    return audioData[index], new_fs

def estimateMFCC(audioData, srate):
    mfcc_data = mfcc(audioData, srate, winlen,winstep,numcep,
         nfilt,nfft,lowfreq,highfreq,preemph,ceplifter,appendEnergy,
         winfunc)
    return mfcc_data

def discardFirstMFCC(mspMFCC, pythonMFCC, discard=1):
    ymspMFCC, ymfcc_feat = [], []
    for i in mspMFCC:
        for j in i[discard:]:
            ymspMFCC.append(j)
    for i in pythonMFCC:
        for j in i[discard:]:
            ymfcc_feat.append(j)
    return ymspMFCC, ymfcc_feat

def estimateMSE(mspData, pyRef):
    return mean_squared_error(pyRef, mspData)

def linePlotMFCC(mspFeat, pyFeat):
    totalMFCC = len(mspFeat)*len(mspFeat[0])
    #print(totalMFCC)
    # x values from 2nd mfcc
    x = [j for j in range(1, totalMFCC+1)]
    
    yMSP, yRef = [], []
    
    for mfccs in range(0, len(mspFeat)):
        for m in range(0, len(mspFeat[0])):
            yMSP.append(mspFeat[mfccs][m])
            yRef.append(pyFeat[mfccs][m])
    
    return yMSP, yRef, x
    
def AudioToTxt():

    for files in testAudioFiles:
        audioPath = testAudioDIR + "/" + files
        savePath = testAudioRawDIR + "/" + files[:-3] + "txt"
        (freq,signal) = wav.read(audioPath)
        
        signal, freq = downSampleAudio(signal, freq, 8000)
        
        # truncate to 1sec recording
        if len(signal) > 8000:
            signal = signal[0:int(1.0 * freq)] 
        elif len(signal) < 8000:
            pad = np.zeros(8000 - len(signal))
            signal = np.append(signal, pad)
            print("{}, {}".format(files, len(pad)))
        sig  = [int(x) for x in signal]

        with open(savePath, 'w') as filehandle:  
           for listitem in sig:
               filehandle.write('%s\n' % listitem)


def createRefMFCC():

    for files in testAudioFiles:
        
        refPath = refMFCCDIR + files[:-3] + "txt"
        rawAudioPath = testAudioRawDIR + "/" + files[:-3] + "txt"
        fileSamples0 = []
        
        with open(rawAudioPath, 'r') as filehandle:  
            for line in filehandle:
                fileSamples0.append(line)
        fileSamples0 = [line.rstrip('\n') for line in fileSamples0]
        
        try:
            fileSamples  = [int(x) for x in fileSamples0]
        except:
            fileSamples  = [int(float(x)) for x in fileSamples0]
            
        fSamples = np.asarray(fileSamples, dtype=np.int16)    
        audioMFCC = estimateMFCC(fSamples ,8000)
       
        audioMFCC = np.delete(audioMFCC, [0], axis=1)
              
        with open(refPath, 'w') as filehandle:  
            for coeff in audioMFCC:
                for m in coeff:
                    filehandle.write('%f ' % m)
                filehandle.write('\n')


def discardFirstMspMFCC():

    for f in rawMspMFCCFiles:
        tmp = readFeatures(f, rawMSPMFCCDIR)
        tmp = np.delete(tmp, [0], axis=1) 
        savePath = mspMFCCDIR + f
        with open(savePath, 'w') as filehandle:  
            for coeff in tmp:
                for m in coeff:
                    filehandle.write('%f ' % m)
                filehandle.write('\n')   

def findMSE():
    
    completed = 0
    mses = []
    fNames = []
    
    print(len(refMFCCFiles))
    
    if len(refMFCCFiles) == len(mspMFCCFiles):
        for f in refMFCCFiles:         
            for ff in mspMFCCFiles:
                if  ff.lower() == f.lower():
                    tmpRef = readFeatures(f, refMFCCDIR)
                    tmpMsp = readFeatures(ff, mspMFCCDIR)
                    mses.append(estimateMSE(tmpMsp, tmpRef))
                    fNames.append(f)
                    print("File: {}, MSE = {}".format(f, estimateMSE(tmpMsp, tmpRef)))
                    completed +=1
    
    with open(statsDIR+"MSE.txt", 'w') as filehandle:  
        for mse in range (0, len(mses)):
            filehandle.write('{} {} {} \n'.format(fNames[mse][:-4], ",", mses[mse]))
            
    
    # plt.title('MSE')
    # plt.plot(xx[:], mses[:], alpha = 0.5)
    #plt.show()


    if completed != len(refMFCCFiles):
        print("MSE failed for {} files".format(len(refMFCCFiles)-completed))


# copies files from dataset to testAudio folder, makes file names shorter using numbers to match short name format required on SD card
def renameAudioDataSetFiles():
    
    for f in range(0, len(testAudioDataSetFiles)):
        fpath = testAudioDataSetDIR + testAudioDataSetFiles[f]
        savePath = testAudioDIR+ "/" + str(f) + ".wav"
        shutil.copyfile(fpath, savePath)
        
    
    
#renameAudioDataSetFiles()
#AudioToTxt()
createRefMFCC()
discardFirstMspMFCC()
findMSE()


tmpRef = readFeatures("PINK.txt", refMFCCDIR)
tmpMsp = readFeatures("PINK.txt", mspMFCCDIR)


print(estimateMSE(tmpMsp, tmpRef))
 
yMSP, yRef, xx = linePlotMFCC(tmpMsp, tmpRef) 
 
smpl = []
 


with open(testAudioRawDIR + "/PINK.txt", 'r') as filehandle:  
    for line in filehandle:
        smpl.append(float(line)) 

# print(smpl[0])

# smpl = smpl[2500:2800]
x = [j for j in range(0, len(smpl))] 

# plt.plot(x[:], smpl[:], alpha = 0.5)
# plt.show()
    
fig, axs = plt.subplots(2)

axs[0].set_title('MFCCs - Pink noise')
axs[1].set_title('Time Domain - Pink noise')
 

#axs[1].title('Audio Samples - Stop')
axs[1].plot(x[:], smpl[:], alpha = 0.5)

#axs[0].title('MFCCs - Stop')
axs[0].plot(xx[:], yRef[:], alpha = 0.5)
axs[0].plot(xx[:], yMSP[:], alpha = 0.5, color='r')
axs[0].legend(['Python','MSP'])

plt.show();


 
# mspMFCC_stop = readFeatures("stop.txt", mspMFCCDIR)


# (freq,sig2) = wav.read("testAudio/stop.wav")
 

# sig2 = sig2[0:int(1.0 * freq)] 

# nine2, new_fs = downSampleAudio(sig2, freq, 8000)
# mfcc_nine2 = estimateMFCC(nine2, new_fs)

 



# with open('mfccs.txt', 'w') as fileh:  
    # for listitem in mfcc_nine2:
        # fileh.write('%s\n' % listitem)


 


# ymspMFCC, ymfcc_feat = discardFirstMFCC(mspMFCC_stop, mfcc_nine2, discard=1)

# # print("MSE = {}".format(estimateMSE(ymfcc_feat, ymspMFCC)))


# alfas = np.arange(0.9, 0.99, 0.01).tolist()
# winsteps = np.arange(0.012, 0.031, 0.001).tolist()
# mse_alfa = []
# mse_winstep = []
# mse_fbanks = []


# for i in alfas:
    # preemph = i
    # mfcc_tmp = estimateMFCC(nine2, new_fs)
    # ymspMFCC, mfcc_tmp = discardFirstMFCC(mspMFCC_stop, mfcc_tmp, discard=1)
    # # print(" alfa = {}, MSE = {}".format(i, estimateMSE(ymfcc_feat, mfcc_tmp)))
    # mse_alfa.append(estimateMSE(ymfcc_feat, mfcc_tmp))


# with open('mse_alfa.txt', 'w') as filehandle:  
    # for i in mse_alfa:
        # filehandle.write('%s\n' % i)
 
# # linePlotMFCC(ymspMFCC, ymfcc_feat)



# # for i in winsteps:
   # # winstep = i
   # # mfcc_tmp = estimateMFCC(nine2, new_fs)
   # # ymspMFCC, mfcc_tmp = discardFirstMFCC(mspMFCC_stop, mfcc_tmp, discard=1)
   # # print(len(mfcc_tmp))
    # # print(" windstep = {}, MSE = {}".format(i, estimateMSE(ymfcc_feat, mfcc_tmp)))
    # # mse_winstep.append(estimateMSE(ymfcc_feat, mfcc_tmp))


# # with open('mse_winstep.txt', 'w') as filehandle:  
   # # for i in mse_winstep:
       # # filehandle.write('%s\n' % i)



# for fb in range (20, 41):
    # nfilt = fb
    # mfcc_tmp = estimateMFCC(nine2, new_fs)
    # ymspMFCC, mfcc_tmp = discardFirstMFCC(mspMFCC_stop, mfcc_tmp, discard=1)
    # # print(" fbank = {}, MSE = {}".format(fb, estimateMSE(ymfcc_feat, mfcc_tmp)))
    # mse_fbanks.append(estimateMSE(ymfcc_feat, mfcc_tmp))

# with open('mse_fbank.txt', 'w') as filehandle:  
    # for i in mse_fbanks:
        # filehandle.write('%s\n' % i)    
        




# with open('./testAudioRaw/pink_noise.txt', 'w') as filehandle:  
   # for listitem in nine2:
       # filehandle.write('%s\n' % listitem)


        
# fig, ax = plt.subplots(2)
# mfcc_python1 = np.swapaxes(mspMFCC_stop, 0 ,1)
# mfcc_python2 = np.swapaxes(mfcc_nine2, 0 ,1)



# # cax = ax[0].imshow(mfcc_msp, interpolation='nearest', cmap=cm.coolwarm, origin='lower')
# cax = ax[0].imshow(mfcc_python1, interpolation='nearest', cmap=cm.coolwarm, origin='lower')

# ax[0].set_title('MSP MFCC - Nine')


# # cax = ax[1].imshow(mfcc_python, interpolation='nearest', cmap=cm.coolwarm, origin='lower')
# cax = ax[1].imshow(mfcc_python2, interpolation='nearest', cmap=cm.coolwarm, origin='lower')

# ax[1].set_title('Python MFCC - Nine')

# # cax = ax[2].imshow(mfcc_python1, interpolation='nearest', cmap=cm.coolwarm, origin='lower')

# # ax[2].set_title('Python MFCC - Stop')

# # cax = ax[3].imshow(mfcc_python2, interpolation='nearest', cmap=cm.coolwarm, origin='lower')

# # ax[3].set_title('Python MFCC - White Noise')


# # plt.show()
        
        

