#!/usr/bin/env python

from python_speech_features import mfcc
from python_speech_features import delta
from python_speech_features import logfbank
import scipy.io.wavfile as wav

import numpy


rate = 8000
winlen=0.032
winstep=0.012
numcep=13
nfilt=26
nfft=256
lowfreq=0
highfreq=None
preemph=0.97
ceplifter=0
appendEnergy=False
winfunc=numpy.hamming



(rate,sig) = wav.read("shortSample.wav")


mfcc_feat = mfcc(sig,rate,winlen,winstep,numcep,
         nfilt,nfft,lowfreq,highfreq,preemph,ceplifter,appendEnergy,
         winfunc)


#d_mfcc_feat = delta(mfcc_feat, 2)
#fbank_feat = logfbank(sig,rate)
#print(fbank_feat[1:3,:])


with open('mfccs.txt', 'w') as fileh:  
    for listitem in mfcc_feat[0]:
        fileh.write('%s\n' % listitem)





